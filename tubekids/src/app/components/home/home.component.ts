import { Component, OnInit } from '@angular/core';
import { PlaylistsService } from 'src/app/services/playlists.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  private playlist: any;

  constructor(private playlistService: PlaylistsService, private authService: AuthService,  private router: Router) {
    playlistService.getPlaylist().then(
      val => {
        this.playlist = val;
      }
    );
  }

  ngOnInit() {
  }

  openPLaylist() {
    if (this.playlist) {
      sessionStorage.setItem('playlist', JSON.stringify(this.playlist));
      this.router.navigate(['/videos']);
    }
  }

  logout(){
    if(confirm('You want to leave TubeKids?')){
      this.authService.logout().then(
        res => {
          if(res){
            localStorage.clear(); 
            sessionStorage.clear(); 
            this.router.navigate(['/users/session']); 
          }
        }, 
        err => alert('Problem to logout')
      )
    }
  }

}
