import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cellcode',
  templateUrl: './cellcode.component.html',
  styleUrls: ['./cellcode.component.scss']
})
export class CellcodeComponent implements OnInit {

  private form: FormGroup;

  constructor(private fb: FormBuilder, private service: AuthService, private router: Router) { }

  ngOnInit() {
    this.form = this.fb.group(
      {
        code: ['', [Validators.required]]
      }
    );
  }

  /**
   * Try to Create a new session.
   */
  onSubmit() {

    if (this.form.valid) {
      this.service.authCellCode(this.form.value).then(
        (value: boolean) => {
          this.router.navigate(['/home']);
        },
        err => {
          alert(err);
        }
      )
    }
  }

}
