import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CellcodeComponent } from './cellcode.component';

describe('CellcodeComponent', () => {
  let component: CellcodeComponent;
  let fixture: ComponentFixture<CellcodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CellcodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CellcodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
