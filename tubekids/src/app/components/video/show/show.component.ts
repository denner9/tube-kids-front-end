import { Component, OnInit, Sanitizer, SecurityContext } from '@angular/core';
import { VideoService } from 'src/app/services/video.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';

export interface VideosInt {
  name: string;
  url: string;
  update: any;
  delete: any
}

@Component({
  selector: 'app-show',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.scss']
})
export class ShowComponent implements OnInit {

  private videosInt: VideosInt[] = [];
  displayedColumns: string[] = ['name', 'url', 'update', 'delete'];
  private videos = [];
  private search = '';

  private playlist: any;

  private htmlVideo = null;

  constructor(private service: VideoService, private sanitizer: DomSanitizer, private router: Router) {
    sessionStorage.setItem('videoUpdate', null);
    this.playlist = JSON.parse(sessionStorage.getItem('playlist'));
    this.getAll();
  }

  ngOnInit() {
  }

  /**
   * FOrmats the YouTube Url
   * @param url URL to convert only Youtube
   */
  convertUrl(url: string) {
    return url.replace('watch?v=', 'embed/');
  }
  /**
   * Changes the video
   * @param element Element choose it
   */
  private changeVideo(element: any) {
    if (!element.url.includes('http')) {
      element.url = element.url.replace('public/', '');
      element.url = 'http://localhost:4200/storage/' + element.url;
      this.htmlVideo = this.sanitizer.bypassSecurityTrustResourceUrl(element.url);
    } else {
      this.htmlVideo = this.sanitizer.bypassSecurityTrustResourceUrl(element.url);
    }
  }

  /**
   * Navigates to add video
   */
  goToAddVideo() {
    if (this.playlist) {
      sessionStorage.setItem('playlist', JSON.stringify(this.playlist));
      this.router.navigate(['/videos/new']);
    }
  }

  /**
   * Go to update window
   * @param element Element choose it
   */
  update(element: any) {
    sessionStorage.setItem('videoUpdate', JSON.stringify(element.update));
    sessionStorage.setItem('playlist', JSON.stringify(this.playlist));

    this.router.navigate(['/videos/new']);
  }

  /**
   * Deletes an element
   * @param element Element to delete
   */
  delete(element: any) {
    if (confirm('You want to delete the video?')) {
      this.service.delete(element.delete).then(
        val => {
          if (val)
            this.getAll();
        },
        err => {
          alert('Problems deleting the video.')
        }
      )
    }
  }

  /**
   * Get all the videos from the DB and reload the table
   */
  private getAll() {
    this.videosInt = [];
    this.service.getAll(this.playlist.id, this.search).then(value => {
      this.videos = value;
      for (let index = 0; index < value.length; index++) {
        this.videosInt.push({
          name: value[index].name,
          url: this.convertUrl(value[index].url),
          update: value[index],
          delete: value[index],
        });

      }
      let url = this.convertUrl(value[0].url);
      this.htmlVideo = this.sanitizer.bypassSecurityTrustResourceUrl(url);
    }
      , err => {
        alert('Problems getting the videos')
      });
  }


}
