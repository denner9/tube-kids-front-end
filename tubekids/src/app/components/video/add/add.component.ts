import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { VideoService } from 'src/app/services/video.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {

  private title = 'Add a video';
  private btntitle = 'Store';
  private state = 'Youtube Video';
  private videoUpdate: any = null;
  private playlist: any;
  private form: FormGroup;

  constructor(private fb: FormBuilder, private service: VideoService, private router: Router) {
    this.videoUpdate = JSON.parse(sessionStorage.getItem('videoUpdate'));
    this.playlist = JSON.parse(sessionStorage.getItem('playlist'));
  }

  ngOnInit() {

    if(this.videoUpdate){
      this.title = 'Update a video'; 
      this.btntitle = 'Update'; 
    }

    this.form = this.fb.group({
      name: new FormControl(this.videoUpdate == null ? '' : this.videoUpdate.name, Validators.required),
      is_youtube: new FormControl(this.videoUpdate == null ? true : this.videoUpdate.is_youtube),
      url: new FormControl(this.videoUpdate == null ? '' : this.videoUpdate.url, Validators.required),
      file: new FormControl(''),
      playlist_id: new FormControl(this.playlist.id, Validators.required)
    });

     

    this.form.get('is_youtube').valueChanges.subscribe(
      value => {
        if (value) {
          this.form.get('url').setValidators(Validators.required);
          this.form.get('file').setValue('');
          this.form.get('file').clearValidators();
          this.form.get('file').clearAsyncValidators();
          this.state = 'Youtube Video';

        } else {
          this.form.get('file').setValidators(Validators.required);
          this.form.get('url').clearValidators();
          this.form.get('url').clearAsyncValidators();
          this.form.get('url').setValue('');
          this.state = 'Local Video';

        }

      }
    );
  }

  onSubmit() {
    if(!this.isValidUrl()){
      return; 
    }
    let data = this.form.value;
    data.is_youtube = data.is_youtube ? 1 : 0;

    if (!data.is_youtube) {
      if (!data.file || data.file == '') {
        alert('You must select a video or change the state to Youtube. ')
      }
    }
    console.log(data);

    if (!this.videoUpdate) {
      this.service.create(data).then(
        val => {
          if (val) {
            alert('Video stored succesful');
            this.router.navigate(['/videos']);

          }
        },
        err => alert('Problems storing the video.')
      )
    } else {
      this.service.update(data, this.videoUpdate.id).then(
        val => {
          if (val) {
            alert('Video updated succesful');
            this.router.navigate(['/videos']);
          }
        },
        err => alert('Problems updating the video. ' + err)
      )
    }
  }

  /**
   * Method activated when the input file selects a file. 
   * @param event Event 
   */
  onFileSelected(event: any) {
    if (event.target.files && event.target.files.length > 0) {
      this.form.get('file').setValue(event.target.files[0]);
    }
  }

  isValidUrl(){
    //https://www.youtube.com/watch?v=-HQOXgapnOA
    let value = this.form.get('url').value;
    if(value.trim() != ''){
      if(!value.includes('youtube.com') || !value.includes('?v=')){
        this.form.get('url').setValue(''); 
        alert('The Youtube URL is not valid.');
        return false; 
      }
    }
    return true; 
  }

}
