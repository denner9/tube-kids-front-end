import { Component, OnInit } from '@angular/core';
import { ProfileService } from 'src/app/services/profile.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-show',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.scss']
})
export class ShowComponent implements OnInit {

  private profiles = [];
  private user: any;

  constructor(private service: ProfileService, private router: Router) {
    this.user = JSON.parse(localStorage.getItem('user'));
  }

  ngOnInit() {
    this.getAll();
  }

  private getAll() {
    this.service.getAll(this.user.id).then(
      value => {
        this.profiles = value;
      }
    );
  }

  /**
   * Delete an item
   * @param item Item select to delete
   */
  private delete(item: any) {
    console.log(item.id)
    if (confirm('Do you want to delete the profile?')) {
      this.service.delete(item).then(
        value => {
          this.getAll();
        }
      )
        .catch(
          err => alert('Problems deleting the profile.')
        )
    }
  }

  /**
   * Go To Update Page
   * @param item item to update
   */
  private update(item: any) {
    sessionStorage.setItem('profileUpdate', JSON.stringify(item)); 
    this.router.navigate(['/profiles/new']); 
  }
}
