import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { ProfileService } from 'src/app/services/profile.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {


  private title = 'Add a profile';
  private btntitle = 'Store';
  private profileUpdate: any = null;
  private form: FormGroup = null;
  private user: any;

  constructor(private fb: FormBuilder, private service: ProfileService, private router: Router) {
    this.user = JSON.parse(localStorage.getItem('user'));
    this.profileUpdate = JSON.parse(sessionStorage.getItem('profileUpdate'));
  }

  ngOnInit() {
    if (this.profileUpdate) {
      this.title = 'Update a profile';
      this.btntitle = 'Update';
    }

    this.form = this.fb.group({
      name: new FormControl(this.profileUpdate == null ? '' : this.profileUpdate.name, Validators.required),
      profile_name: new FormControl(this.profileUpdate == null ? '' : this.profileUpdate.profile_name),
      pin: new FormControl(this.profileUpdate == null ? '' : this.profileUpdate.pin, [Validators.required, Validators.maxLength(6), Validators.minLength(6)]),
      birth: new FormControl(''),
      user_id: new FormControl(this.user.id)
    });
  }

  onSubmit() {
    if (!this.profileUpdate) {
      if (this.form.valid) {
        this.service.create(this.form.value).then(
          res => {
            alert('Profile created');
            this.router.navigate(['/profiles']);
          },
          err => {
            alert('Problems creating the profile.');
          }
        )
      }
    } else {
      if (this.form.valid) {
        this.service.update(this.form.value, this.profileUpdate.id).then(
          res => {
            alert('Profile updated');
            this.router.navigate(['/profiles']);
          },
          err => {
            alert('Problems updating the profile.');
          }
        )
      }
    }
  }

}
