import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  private form: FormGroup; 

  constructor(private fb: FormBuilder, private service: AuthService, private router: Router) { 
    localStorage.clear(); 
    sessionStorage.clear();
  }

  ngOnInit() {
    this.form = this.fb.group(
      {
        email: ['', [Validators.required, Validators.email]],
        password: ['', [Validators.required]]
      }
    );
  }

  /**
   * Try to Create a new session.
   */
  onSubmit(){

    if(this.form.valid){
      this.service.createSession(this.form.value).then(
        value => {
          if (value){
            //redirect to code
            this.router.navigateByUrl('/users/session/code'); 
          }
        }, 
        err => {
          alert('Email or password incoorect. If you have not verified your account in your email, please do it.');
        }
      )
    }
  }

}
