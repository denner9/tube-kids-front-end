import { Component, OnInit, ɵConsole } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { COUNTRIES } from '../../shared/constants';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  form: FormGroup;

  private countries = COUNTRIES;

  constructor(private fb: FormBuilder, private service: UserService, private router: Router) {

  }

  ngOnInit() {
    this.form = this.fb.group({
      name: ['', [Validators.required]],
      lastname: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      phone: ['', [Validators.required]],
      password: ['', [Validators.required]],
      password_repeat: ['', [Validators.required]],
      country: [this.countries[0], [Validators.required]],
      birth: [new Date(), [Validators.required]],
    }
    );
  }

  /**
   * Method to executes the forms action. It will save the user.
   */
  private onSubmit() {

    if (this.form.valid) {
      this.service.create(this.form.value).then(
        res => {
          if (!res)
            alert('There was systen errors. Please contact support@tubekids.com');
          else
            alert('User created.');
            this.router.navigate(['/users/session']);
        },
        err => {
          alert(err + '. Please contact support@tubekids.com');
        }
      )
      return;
    }

    alert('The form is invalid. Please verify the spaces.');

  }

}
