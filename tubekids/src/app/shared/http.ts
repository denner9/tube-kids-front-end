import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { BASE_URL } from '../shared/constants';

@Injectable()
export class Http {

    constructor(private http: HttpClient, private router: Router) { }

    /**
     * GET Http Method
     * @param url URL to make a GET request. 
     * @returns Promise
     */
    get(url: string): Observable<HttpResponse<any>> {
        return this.http.get<any>(url, { headers: this.token(), observe: "response" });
    }

    /**
     * POST Http Method
     * @param url URL to make a POST request. 
     * @param data Data type object {key:value}
     * @returns Promise
     */
    post(url: string, data: any): Observable<HttpResponse<any>> {
        return this.http.post<any>(url, this.params(data), { headers: this.token(), observe: "response" });
    }

    /**
     * PUT Http Method
     * @param url URL to make a PUT request. 
     * @param data Data type object {key:value}
     * @returns Promise
     */
    put(url: string, data: any): Observable<HttpResponse<any>> {
        return this.http.put<any>(url, this.params(data), { headers: this.token(), observe: "response" });;
    }

    /**
     * DELETE Http Method
     * @param url URL to make a DELETE request. 
     * @param data Data type object {key:value}
     * @returns Promise
     */
    delete(url: string): Observable<HttpResponse<any>> {
        return this.http.delete<any>(url, { headers: this.token(), observe: "response" });

    }

    private renewToken() {
        this.http.post<any>(BASE_URL + "users/session/new", { headers: this.token(), observe: "response" })
            .subscribe(
                res => {
                    if (res.status == 201) {
                        return res.body;
                    }
                }, err => console.warn(err)
            );
    }

    /**
     * Process the erros catched on the promise
     * @param err Error get it
     */
    catch(err: any) {
        try {
            console.warn(err);
            const error: number = JSON.parse(err.status);
            switch (error) {
                case 401:
                    localStorage.clear();
                    sessionStorage.clear();
                    this.router.navigate(['/users/session']);
                    break;
                case 422:
                    return "Invalid form data. Please confirm especially the email and the birthdate must be 18 years old or older."
                case 500:
                    if (err.error.code == 23000) {
                        return "The email already exits, please use another one.";
                    }
                    return "Server error. Contact support@glo-ware.com";
                case 403:
                case 406:
                    return err.error.msg;
            }
        } catch (error) {
            console.error(err);
        }
    }

    /**
     * Method to process the params en capsulate them/it in the correct format fot a post
     * @param data Data type object {key:value}
     * @returns Object with the HttpParams
     */
    private params(data: any) {
        if (data instanceof FormData) {
            return data
        }
        let httpParams = new HttpParams();
        Object.keys(data).forEach(function (key) {
            httpParams = httpParams.append(key, data[key]);;
        });
        return httpParams;
    }

    /**
     * Method to get the token and set it on a Header.
     * @returns Object with the HttpHeaders
     */
    private token() {
        return new HttpHeaders({ 'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('token')) });
    }
}