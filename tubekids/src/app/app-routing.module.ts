import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';
import { CellcodeComponent } from './components/cellcode/cellcode.component';
import { HomeComponent } from './components/home/home.component';
import { IsLoggedGuard } from './guards/is-logged.guard';
import { CanGoLoginGuard } from './guards/can-go-login.guard';
import { CanGoCellCodeGuard } from './guards/can-go-cell-code.guard';
import { AddComponent } from './components/video/add/add.component';
import { IsNotLoggedGuardGuard } from './guards/is-not-logged-guard.guard';
import { ShowComponent } from './components/video/show/show.component';
import { ShowComponent as Show } from './components/profile/show/show.component';
import { ProfilesComponent } from './components/profile/profiles/profiles.component';
import { CreateComponent } from './components/profile/create/create.component';

const routes: Routes = [
  { path: 'users/new', component: RegisterComponent },
  { path: 'users/session', component: LoginComponent, canActivate: [IsNotLoggedGuardGuard] },
  { path: 'users/session/code', component: CellcodeComponent, canActivate: [CanGoCellCodeGuard] },
  { path: 'home', component: HomeComponent, canActivate: [IsLoggedGuard] },
  { path: 'videos/new', component: AddComponent, canActivate: [IsLoggedGuard] },
  { path: 'videos', component: ShowComponent, canActivate: [IsLoggedGuard] },
  {
    path: 'profiles', component: ProfilesComponent, canActivate: [IsLoggedGuard], children: [
      {path: 'new', component: CreateComponent},
      {path: 'list', component: Show},
      { path: '', redirectTo: '/profiles/list', pathMatch: 'full' }
    ]
  },


  { path: '', redirectTo: '/home', pathMatch: 'full' }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
