import { Injectable } from '@angular/core';
import { Http } from "../shared/http";
import { BASE_URL } from "../shared/constants";
import { HttpResponse } from '@angular/common/http';
import { DatePipe } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor(private http: Http) { }

  /**
   * API Request for store a profile
   * @param profile Profile get it from the form value. 
   * @returns Promise<any> resolve(true) and the reject give a error message
   */
  create(profile: any) {
    if(profile.birth)
      profile.birth = new DatePipe("en-US").transform(profile.birth, "yyyy-MM-dd HH:mm:SS");
    
    return new Promise<any>((resolve, reject) => {
      this.http.post(BASE_URL + 'profiles', profile).subscribe(
        (response: HttpResponse<any>) => {
          console.log(response);
          if (response.status == 201) {
            //Created
            resolve(true);
          }
        },
        err => {
          console.warn(err);
          reject([false, this.http.catch(err)]);
        }
      )
    })
  }

  /**
   * Get all the profiles
   * @param userid Current User ID
   */
  getAll(userid:number) {
    let uri = `?userid=${userid}`;

    return new Promise<any>((resolve, reject) => {
      this.http.get(BASE_URL + 'profiles/' + uri).subscribe(
        (response: HttpResponse<any>) => {
          if (response.status == 200) {
            resolve(response.body);

            console.log(response.body)
          }
        },
        err => {
          console.warn(err);
          reject([false, this.http.catch(err)]);
        }
      )
    });
  }

  /**
   * Delete a profile
   * @param profile Profile to delete
   */
  delete(profile: any) {
    return new Promise<any>((resolve, reject) => {
      this.http.delete(BASE_URL + 'profiles/' + profile.id).subscribe(
        (response: HttpResponse<any>) => {
          if (response.status == 204) {
            resolve(true);
          }
        },
        err => {
          console.warn(err);
          reject([false, this.http.catch(err)]);
        }
      );
    });
  }

  /**
   * API Request for update a video
   * @param profile Video get it from the form value. 
   * @param user_id Profile ID to update 
   * @returns Promise<any> resolve(true) and the reject give a error message
   */
  update(profile: any, user_id: number) {
    if(profile.birth)
      profile.birth = new DatePipe("en-US").transform(profile.birth, "yyyy-MM-dd HH:mm:SS");
    
    return new Promise<any>((resolve, reject) => {
      this.http.put(BASE_URL + 'profiles/' + user_id, profile).subscribe(
        (response: HttpResponse<any>) => {
          console.log(response);
          if (response.status == 200) {
            //Created
            resolve(true);
          }
        },
        err => {
          console.warn(err);
          reject([false, this.http.catch(err)]);
        }
      )
    })
  }
}
