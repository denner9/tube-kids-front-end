import { Injectable } from '@angular/core';
import { Http } from "../shared/http";
import { BASE_URL } from "../shared/constants";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: Http) { }

  /**
   * First step user auth
   * @param formValue Form Values 
   * @returns Promise<any> resolve: boolean confirming if the login was ok for the second auth.
   * reject: error string
   */
  public createSession(formValue: any) {
    return new Promise<any>((resolve, reject) => {
      if (formValue.password == '') {
        reject('The password field is empty.');
      } else {
        this.http.post(BASE_URL + 'users/session', formValue).subscribe(
          (response: any) => {
            if (response.status == 201 && response.body.logged) {
              sessionStorage.setItem('logged', JSON.stringify(response.body.logged));
              localStorage.setItem('user', JSON.stringify(response.body.user));
              resolve(true);
            }
          },
          err => {
            console.warn(err);
            reject(this.http.catch(err));
          }
        )
      }

    });
  }

  /**
   * Validates the cellphone token in the API 
   * @param formValue Form value with the code
   */
  public authCellCode(formValue: any) {

    let data = {
      token: formValue.code,
      user_id: JSON.parse(localStorage.getItem('user')).id
    }
    console.log(data);
    return new Promise<any>((resolve, reject) => {
      if (data.token == '') {
        reject('The cellphone code is empty.');
      } else if (data.user_id == null) {
        reject('There is no logged user')
      }
      else {
        this.http.post(BASE_URL + 'users/session/code', data).subscribe(
          (response: any) => {

            localStorage.setItem('token', JSON.stringify(response.body.access_token));
            localStorage.setItem('user', JSON.stringify(response.body.user));
            sessionStorage.clear();
            console.log(response.body)
            resolve(true)

          },
          err => {
            console.warn(err);
            reject(this.http.catch(err));
          }
        )
      }

    });
  }

  /**
   * Logout from the app. 
   * @returns Promise<any>((resolve => true , reject => )
   */
  public logout() {
    return new Promise<any>((resolve, reject) => {

      this.http.delete(BASE_URL + 'users/session').subscribe(
        (response: any) => {
          if (response.status == 204) {
            resolve(true);
          }
        },
        err => {
          console.warn(err);
          reject(this.http.catch(err));
        }
      )

    });
  }
}
