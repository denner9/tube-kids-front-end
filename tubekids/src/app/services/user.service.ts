import { Injectable } from '@angular/core';
import { Http } from "../shared/http";
import { BASE_URL } from "../shared/constants";
import { DatePipe } from "@angular/common";
import { PlaylistsService } from './playlists.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: Http, private playlistService: PlaylistsService) { }

  /**
   * Make the request to the API to create a new user.
   * @param userData Form data value.
   * @return Promise<any>(resolve -> true, reject -> error message).
   */
  public create(userData: any) {
    let data = {
      email: userData.email,
      name: userData.name,
      lastname: userData.lastname,
      country_code:  userData.country.code,
      verify_password: userData.password_repeat,
      phone: userData.phone,
      birth: new DatePipe("en-US").transform(userData.birth, "yyyy-MM-dd HH:mm:SS"), 
      password: userData.password
    };
    console.log(data); 
    return new Promise<any>((resolve, reject) => {
      if (data.password == !data.verify_password) {
        reject('The passwords are diferents.');
      } else {
        this.http.post(BASE_URL + 'users', data).subscribe(
          (response: any) => {
            if (response.status == 201) {
              resolve(true); 
              console.log(response.body); 
              this.playlistService.createPlaylist(response.body[0].id).catch(
                err => {
                  reject(err[1]); 
                }
              )
            }
          },
          err => {
            console.warn(err);
            reject(this.http.catch(err));
          }
        )
      }

    });
  }
}
