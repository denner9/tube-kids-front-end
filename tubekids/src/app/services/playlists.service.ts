import { Injectable } from '@angular/core';
import { Http } from "../shared/http";
import { BASE_URL } from "../shared/constants";

@Injectable({
  providedIn: 'root'
})
export class PlaylistsService {

  constructor(private http: Http) { }

  /**
   * Method for create the playlist for a user
   * @param uid User's id
   */
  public createPlaylist(uid: number) {
    return new Promise<any>((resolve, reject) => {
      this.http.post(BASE_URL + 'playlists', { name: 'General', user_id: uid }).subscribe(
        (response: any) => {
          resolve([response.status == 201, 'Ok']);
        },
        err => {
          console.warn(err);
          reject([false, this.http.catch(err)]);
        }
      );
    })
  }

  /**
   * Get the playlist of the user
   * @returns Promise<any> resolve (the playlist object) reject (error message)
   */
  public getPlaylist() {
    let user = JSON.parse(localStorage.getItem('user'));
    console.log(BASE_URL + "playlists/" + user.id);
    return new Promise<any>((resolve, reject) => {
      this.http.get(BASE_URL + "playlists/" + user.id).subscribe(
        response => {
          if (response.status == 200) {
            console.log(response.body);
            resolve(response.body);
          }
        },
        err => {
          console.warn(err);
          reject([false, this.http.catch(err)]);
        }

      )
    });
  }

}
