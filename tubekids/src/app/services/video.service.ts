import { Injectable } from '@angular/core';

import { Http } from "../shared/http";
import { BASE_URL } from "../shared/constants";
import { HttpResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class VideoService {

  constructor(private http: Http) { }

  /**
   * API Request for store a video
   * @param video Video get it from the form value. 
   * @returns Promise<any> resolve(true) and the reject give a error message
   */
  create(video: any) {

    let resource = new FormData();
    resource.append('is_youtube', video.is_youtube);
    resource.append('playlist_id', video.playlist_id);
    resource.append('url', video.url);
    resource.append('file_name', video.file.name);
    resource.append('name', video.name);
    if (!video.is_youtube)
      resource.append('file', video.file, video.file.name);

    console.log(video.file);

    return new Promise<any>((resolve, reject) => {
      setTimeout(() => {
        this.http.post(BASE_URL + 'videos', resource).subscribe(
          (response: HttpResponse<any>) => {
            console.log(response);
            if (response.status == 201) {
              //Created
              resolve(true);
            }
          },
          err => {
            console.warn(err);
            reject([false, this.http.catch(err)]);
          }
        )
      }, 500);
    })
  }

  /**
   * Get all the videos
   * @param playlistId Playlist ID
   */
  getAll(playlistId: number, filter: string) {
    let uri = `?playlist_id=${playlistId}`;

    if (filter.trim() != '') {
      uri += `&name=${filter}`;
    }

    return new Promise<any>((resolve, reject) => {
      this.http.get(BASE_URL + 'videos/' + uri).subscribe(
        (response: HttpResponse<any>) => {
          if (response.status == 200) {
            resolve(response.body);

            console.log(response.body)
          }
        },
        err => {
          console.warn(err);
          reject([false, this.http.catch(err)]);
        }
      )
    });
  }

  /**
   * Delete a video
   * @param video Video to delete
   */
  delete(video: any) {
    return new Promise<any>((resolve, reject) => {
      this.http.delete(BASE_URL + 'videos/' + video.id).subscribe(
        (response: HttpResponse<any>) => {
          if (response.status == 204) {
            resolve(true);
          }
        },
        err => {
          console.warn(err);
          reject([false, this.http.catch(err)]);
        }
      );
    });
  }

  /**
   * API Request for update a video
   * @param video Video get it from the form value. 
   * @returns Promise<any> resolve(true) and the reject give a error message
   */
  update(video: any, video_id: number) {

    // let resource = new FormData();
    // resource.append('is_youtube', video.is_youtube);
    // resource.append('playlist_id', video.playlist_id);
    // resource.append('url', video.url);
    // resource.append('name', video.name);
    // if (!video.is_youtube){
    //   resource.append('file', video.file, video.file.name);
    //   resource.append('file_name', video.file.name);
    // }

    return new Promise<any>((resolve, reject) => {
      setTimeout(() => {
        this.http.put(BASE_URL + 'videos/' + video_id, video).subscribe(
          (response: HttpResponse<any>) => {
            console.log(response);
            if (response.status == 200) {
              //Created
              resolve(true);
            }
          },
          err => {
            console.warn(err);
            reject([false, this.http.catch(err)]);
          }
        )
      }, 1000);
    })
  }


}
