import { TestBed, async, inject } from '@angular/core/testing';

import { IsNotLoggedGuardGuard } from './is-not-logged-guard.guard';

describe('IsNotLoggedGuardGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IsNotLoggedGuardGuard]
    });
  });

  it('should ...', inject([IsNotLoggedGuardGuard], (guard: IsNotLoggedGuardGuard) => {
    expect(guard).toBeTruthy();
  }));
});
