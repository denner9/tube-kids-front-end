import { TestBed, async, inject } from '@angular/core/testing';

import { CanGoCellCodeGuard } from './can-go-cell-code.guard';

describe('CanGoCellCodeGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CanGoCellCodeGuard]
    });
  });

  it('should ...', inject([CanGoCellCodeGuard], (guard: CanGoCellCodeGuard) => {
    expect(guard).toBeTruthy();
  }));
});
