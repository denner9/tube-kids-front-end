import { TestBed, async, inject } from '@angular/core/testing';

import { CanGoLoginGuard } from './can-go-login.guard';

describe('CanGoLoginGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CanGoLoginGuard]
    });
  });

  it('should ...', inject([CanGoLoginGuard], (guard: CanGoLoginGuard) => {
    expect(guard).toBeTruthy();
  }));
});
