import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { Http } from './shared/http';


import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatDatepickerModule, MatTableModule, MatFormFieldModule, MatNativeDateModule, MatInputModule, MatSlideToggleModule} from '@angular/material';



import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';
import { CellcodeComponent } from './components/cellcode/cellcode.component';
import { HomeComponent } from './components/home/home.component';
import { AddComponent } from './components/video/add/add.component';
import { ShowComponent } from './components/video/show/show.component';
import { CreateComponent } from './components/profile/create/create.component';
import { ProfilesComponent } from './components/profile/profiles/profiles.component';
import { ShowComponent as Show } from './components/profile/show/show.component';

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    CellcodeComponent,
    HomeComponent,
    AddComponent,
    ShowComponent,
    CreateComponent,
    ProfilesComponent,
    Show
  ],
  imports: [
    BrowserModule,
    AppRoutingModule, 
    RouterModule, 
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatSlideToggleModule,
    MatTableModule,
  ],
  providers: [
    FormBuilder,
    Http
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
